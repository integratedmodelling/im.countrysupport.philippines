worldview im;

@test(
    name = "test_original_crs_impact_case_g",
    description  = "",
    // add to the list any concepts to be observed in the context below
    observations = (""),
    // add assertions to check the observations after execution
    assertions = ()
)
// test case 7: 4326|1000mt|32651|32651
observe earth:Region named test_case_7
     over space(urn='local:alessio.bulckaen:im.countrysupport.philippines:im-nca-philippines_gadm36_phl_0',
	       grid= '1000 m',
	       projection='EPSG:32651'),
	      time(start=2010, end=2020, step=5.year);

// Landcover type resolver
//EPSG32651	
model local:alessio.bulckaen:im.countrysupport.philippines:im-nca-philippines.lc_32651_50m_vnd_2010,
	  local:alessio.bulckaen:im.countrysupport.philippines:im-nca-philippines.lc_32651_50m_vnd_2015
	  as landcover:LandCoverType classified into
		landcover:OpenMixedForest if 1,
		landcover:InlandSwamp if 2,
		landcover:Shrubland if 3,
		landcover:BareArea if 4,
		landcover:Grassland if 5,
		landcover:NonIrrigatedArableLandHerbaceous if 6,
		landcover:PermanentCropland if 7,
		landcover:InlandWaterBody if 8,
		landcover:ArtificialSurface if 9,
		landcover:WaterBody  if 10,
		landcover:ClosedMixedForest if 11,
		landcover:Wetland if 12,
		landcover:SeaAndOcean if 13;

//Net Change table		
define table landcover_basic as {
	title: "Selected land cover types",
	label: "Net change in land cover",
	target: landcover:LandCoverType	
	columns: ({ title:  "{classifier}", target: im:Area in km^2, filter: landcover:LandCoverType 	
	}
	{ 
		title: "Total", summarize: sum, style: bold
	}),
	rows: (
		{ title: "Area {time} (km²)", filter: (start end) } 
		{ title: "Net change", summarize: [r2 - r1], style: bold }
	)
};
