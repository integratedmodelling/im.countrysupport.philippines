worldview im;

@test(
    name = "region_eight_test_extentworldclimatedata",
    description  = "Test extetn model using World climate data instead of Copernicus",
    // add to the list any concepts to be observed in the context below
    observations = ("philippines.landcover.land-cover-type-resolver",
    				"seea.aries.tables.ecosystemextent.extents_basic"),
    // add assertions to check the observations after execution
    assertions = ()
)

observe earth:Region named philippines_r8_gadm_30mt_32651
     over space(urn='local:alessio.bulckaen:im.countrysupport.philippines:im-nca-philippines_r8_boundary2020_wgs84utm51n_gadm0',
	       grid= '1000 m',
	       projection='EPSG:32651'),
	      time(start=2015, end=2022, step=1.year);
//		  time(year=2015);

////////////////////////
// WorldClim 2.1
////////////////////////
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_avg_temp_30s_1970_2000_01' as earth:AtmosphericTemperature during im:January   in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_avg_temp_30s_1970_2000_02' as earth:AtmosphericTemperature during im:February  in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_avg_temp_30s_1970_2000_03' as earth:AtmosphericTemperature during im:March     in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_avg_temp_30s_1970_2000_04' as earth:AtmosphericTemperature during im:April     in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_avg_temp_30s_1970_2000_05' as earth:AtmosphericTemperature during im:May       in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_avg_temp_30s_1970_2000_06' as earth:AtmosphericTemperature during im:June      in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_avg_temp_30s_1970_2000_07' as earth:AtmosphericTemperature during im:July      in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_avg_temp_30s_1970_2000_08' as earth:AtmosphericTemperature during im:August    in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_avg_temp_30s_1970_2000_09' as earth:AtmosphericTemperature during im:September in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_avg_temp_30s_1970_2000_10' as earth:AtmosphericTemperature during im:October   in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_avg_temp_30s_1970_2000_11' as earth:AtmosphericTemperature during im:November  in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_avg_temp_30s_1970_2000_12' as earth:AtmosphericTemperature during im:December  in Celsius;

@intensive(time)
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_solar_radiation_30s_1970_2000_01' as earth:SolarRadiation during im:January  in J/m^2*s; 
@intensive(time)
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_solar_radiation_30s_1970_2000_02' as earth:SolarRadiation during im:February in J/m^2*s;
@intensive(time)
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_solar_radiation_30s_1970_2000_03' as earth:SolarRadiation during im:March    in J/m^2*s;
@intensive(time)
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_solar_radiation_30s_1970_2000_04' as earth:SolarRadiation during im:April    in J/m^2*s;
@intensive(time)
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_solar_radiation_30s_1970_2000_05' as earth:SolarRadiation during im:May      in J/m^2*s;
@intensive(time)
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_solar_radiation_30s_1970_2000_06' as earth:SolarRadiation during im:June     in J/m^2*s;
@intensive(time)
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_solar_radiation_30s_1970_2000_07' as earth:SolarRadiation during im:July     in J/m^2*s;
@intensive(time)
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_solar_radiation_30s_1970_2000_08' as earth:SolarRadiation during im:August   in J/m^2*s;
@intensive(time)
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_solar_radiation_30s_1970_2000_09' as earth:SolarRadiation during im:September in J/m^2*s;
@intensive(time)
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_solar_radiation_30s_1970_2000_10' as earth:SolarRadiation during im:October  in J/m^2*s;
@intensive(time)
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_solar_radiation_30s_1970_2000_11' as earth:SolarRadiation during im:November in J/m^2*s;
@intensive(time)
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_solar_radiation_30s_1970_2000_12' as earth:SolarRadiation during im:December in J/m^2*s;

model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_min_temp_30s_1970_2000_01' as im:Minimum earth:AtmosphericTemperature during im:January  in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_min_temp_30s_1970_2000_02' as im:Minimum earth:AtmosphericTemperature during im:February in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_min_temp_30s_1970_2000_03' as im:Minimum earth:AtmosphericTemperature during im:March    in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_min_temp_30s_1970_2000_04' as im:Minimum earth:AtmosphericTemperature during im:April    in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_min_temp_30s_1970_2000_05' as im:Minimum earth:AtmosphericTemperature during im:May      in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_min_temp_30s_1970_2000_06' as im:Minimum earth:AtmosphericTemperature during im:June     in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_min_temp_30s_1970_2000_07' as im:Minimum earth:AtmosphericTemperature during im:July     in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_min_temp_30s_1970_2000_08' as im:Minimum earth:AtmosphericTemperature during im:August   in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_min_temp_30s_1970_2000_09' as im:Minimum earth:AtmosphericTemperature during im:September in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_min_temp_30s_1970_2000_10' as im:Minimum earth:AtmosphericTemperature during im:October  in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_min_temp_30s_1970_2000_11' as im:Minimum earth:AtmosphericTemperature during im:November in Celsius;
model 'local:rubencc:im.data.global:im-data-global-climate.worldclim_21_min_temp_30s_1970_2000_12' as im:Minimum earth:AtmosphericTemperature during im:December in Celsius;
