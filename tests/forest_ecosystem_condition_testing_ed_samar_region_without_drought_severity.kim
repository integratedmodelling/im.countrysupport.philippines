worldview im;

@test(
    name = "forest.ecosystem.condition.testing.ed.samar_region_without_drought_severity",
   description  = "Forest Ecosystem Condition current model with reference cut-off, normalization and overall condition computed using euclidean distance",
    observations = (
    				"philippines.landcover.land-cover-type-resolver",
    				"seea.aries.tables.ecosystemextent.extents_additions_reductions"
    				"im:Indicator ecology:ForestFragmentation",
					"im:Indicator ecology:NormalizedDifferenceVegetationIndex",
					"im:Indicator ecology:NetPrimaryProduction",
					"im:Indicator ecology:LeafAreaIndex",
    				"im:Indicator im:Mean es.nca:Condition"
    				"im|im.countrysupport.philippines/tests/forest_ecosystem_condition_testing_referenceandnormalization_am_samar_region.testing_condition_index_dynamic"
					),
    assertions = ()
	)
observe earth:Region named samar_region_forest_ecosystem_condition_testing
     over space(urn='local:alessio.bulckaen:im.countrysupport.philippines:im-nca-philippines_gadm41_phl_1#name_1=Eastern Samar',
	       grid= '2000 m',
	       projection='EPSG:32651'),
 	time(start=2015, end=2021, step=1.year);

private model im:Reference ecology:NormalizedDifferenceVegetationIndex 
		observing 
		ecology:NormalizedDifferenceVegetationIndex where (es.nca:EcosystemType = es.nca:ForestEcosystem) named forest_ndvi,
		im:Reference presence of conservation:ProtectedArea named presence_of_reference_protected_area
	set to [ presence_of_reference_protected_area ? forest_ndvi : unknown];

private model im:Reference ecology:LeafAreaIndex
		observing 
		ecology:LeafAreaIndex where (es.nca:EcosystemType = es.nca:ForestEcosystem)	named forest_lai,
		im:Reference presence of conservation:ProtectedArea named presence_of_reference_protected_area
	set to [presence_of_reference_protected_area ? forest_lai : unknown];

@intensive(space)  
@extensive(time)  
private model im:Reference ecology:NetPrimaryProduction in t/ha
		observing 
		ecology:NetPrimaryProduction in t/ha where (es.nca:EcosystemType = es.nca:ForestEcosystem) named forest_npp, 
		im:Reference presence of conservation:ProtectedArea named presence_of_reference_protected_area
	set to [presence_of_reference_protected_area ? forest_npp : unknown];

private model im:Reference ecology:ForestFragmentation
		observing 
		ecology:ForestFragmentation named forest_fragmentation,
		im:Reference presence of conservation:ProtectedArea  named presence_of_reference_protected_area
	set to [presence_of_reference_protected_area ? forest_fragmentation : unknown];
	
private model im:Reference soil:DroughtSeverity
		observing 
		soil:DroughtSeverity where (es.nca:EcosystemType = es.nca:ForestEcosystem) named forest_drought_severity,
		im:Reference presence of conservation:ProtectedArea named presence_of_reference_protected_area
	set to [presence_of_reference_protected_area ? forest_drought_severity : unknown];


@colormap(colors=(red yellow green), midpoint=0)	
private model im:Indicator ecology:NormalizedDifferenceVegetationIndex 	
	observing 
		im:Reference ecology:NormalizedDifferenceVegetationIndex named minimum_disturbance_ndvi,
        ecology:NormalizedDifferenceVegetationIndex where (es.nca:EcosystemType = es.nca:ForestEcosystem) named forest_ndvi
	set to [
		    def temp_ndvi = (forest_ndvi > minimum_disturbance_ndvi.avg ? minimum_disturbance_ndvi.avg : forest_ndvi)
            return (temp_ndvi - minimum_disturbance_ndvi.avg) ],
		klab.data.normalize();

@colormap(colors=(red yellow green), midpoint=0)
private model im:Indicator ecology:LeafAreaIndex	
	observing 
		im:Reference ecology:LeafAreaIndex named minimum_disturbance_lai,
        ecology:LeafAreaIndex where (es.nca:EcosystemType = es.nca:ForestEcosystem) named forest_lai
	set to [
		def temp_lai = (forest_lai > minimum_disturbance_lai.avg ? minimum_disturbance_lai.avg : forest_lai)
            return (temp_lai - minimum_disturbance_lai.avg) ],
		klab.data.normalize();

@intensive(space)  
@extensive(time)
@colormap(colors=(red yellow green), midpoint=0)
private model im:Indicator ecology:NetPrimaryProduction
	observing
		im:Reference ecology:NetPrimaryProduction in t/ha named minimum_disturbance_npp,
        ecology:NetPrimaryProduction in t/ha where (es.nca:EcosystemType = es.nca:ForestEcosystem) named forest_npp
	set to [
		def temp_npp = (forest_npp > minimum_disturbance_npp.avg ? minimum_disturbance_npp.avg : forest_npp)
            return (temp_npp - minimum_disturbance_npp.avg) ],
		klab.data.normalize();

@colormap(colors=(red yellow green), midpoint=0)		
private model im:Indicator ecology:ForestFragmentation
	observing 
		im:Reference ecology:ForestFragmentation named minimum_disturbance_fragmentation,
        ecology:ForestFragmentation where (es.nca:EcosystemType = es.nca:ForestEcosystem) named fragmentation_over_forest
	set to [
		def temp_fragmentation_over_forest = (fragmentation_over_forest < minimum_disturbance_fragmentation.avg ? minimum_disturbance_fragmentation.avg : fragmentation_over_forest)
            return (minimum_disturbance_fragmentation.avg - temp_fragmentation_over_forest)],
		klab.data.normalize();
		
		
//testing euclidean distance
@colormap(colors=(red yellow green), midpoint=0)
@parameter(name=weight_1, default=0.2, label="Weight_NDVI", description = "")
@parameter(name=weight_2, default=0.2, label="Weight_LAI", description = "")
@parameter(name=weight_3, default=0.2, label="Weight_NPP", description = "")
@parameter(name=weight_4, default=0.2, label="Weight_Fragmentation", description = "")

model im:Indicator im:Mean es.nca:Condition
	observing
		im:Indicator ecology:NormalizedDifferenceVegetationIndex named ndvi_indicator,
		im:Indicator ecology:LeafAreaIndex named lai_indicator,
		im:Indicator ecology:NetPrimaryProduction named npp_indicator, 
		im:Indicator ecology:ForestFragmentation named fragmentation_indicator
	set to [
		drought_indicator = (nodata(drought_indicator) ? 0 : drought_indicator)
		def temp_ind = weight_1 * Math.pow(ndvi_indicator,2) + weight_2 * Math.pow(lai_indicator,2) + weight_3 * Math.pow(npp_indicator,2) + weight_4 * Math.pow(fragmentation_indicator,2)
		def ret = Math.sqrt(temp_ind)
		return ret
	];
